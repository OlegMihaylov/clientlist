package cvdevelopers.takehome.entity

data class ApiResponse (
    val results: List<Client>
)