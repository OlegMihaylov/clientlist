package cvdevelopers.takehome.entity


data class Id(
    val name: String,
    val value: String
)