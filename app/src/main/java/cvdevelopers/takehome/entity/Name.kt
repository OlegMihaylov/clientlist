package cvdevelopers.takehome.entity

data class Name(
    val first: String,
    val last: String,
    val title: String
)