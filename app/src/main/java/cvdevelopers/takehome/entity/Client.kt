package cvdevelopers.takehome.entity

data class Client(
        val id: Id,
        val name: Name,
        val email: String,
        val picture: Picture
)