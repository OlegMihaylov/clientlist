package cvdevelopers.takehome.model.data.server

import cvdevelopers.takehome.entity.ApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RandomUserApiEndpoint {

  @GET("api")
  fun getClient(
          @Query("page") page: Int,
          @Query("results") results: Int
  ): Single<ApiResponse>

}