package cvdevelopers.takehome.model.data.storage

import cvdevelopers.takehome.entity.Client
import io.reactivex.Completable
import io.reactivex.Single

interface LocalClientRepository {

    fun saveClientList(clientList: List<Client>): Completable

    fun getClientList(): Single<List<Client>>

}