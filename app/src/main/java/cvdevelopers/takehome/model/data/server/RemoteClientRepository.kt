package cvdevelopers.takehome.model.data.server

import cvdevelopers.takehome.entity.Client
import io.reactivex.Single

interface RemoteClientRepository {

    fun getClientList(page: Int, pageSize: Int): Single<List<Client>>

}