package cvdevelopers.takehome.model.data.storage.room

import androidx.room.*
import io.reactivex.Single

@Dao
interface ClientDao {

    @Query("DELETE FROM client")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveClientList(list: List<DatabaseClient>)

    @Transaction
    fun updateClients(list: List<DatabaseClient>) {
        deleteAll()
        saveClientList(list)
    }

    @Query("SELECT * FROM client")
    fun getClientList(): Single<List<DatabaseClient>>

}