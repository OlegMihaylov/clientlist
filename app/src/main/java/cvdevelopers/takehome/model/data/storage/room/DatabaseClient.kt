package cvdevelopers.takehome.model.data.storage.room

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "client")
data class DatabaseClient(
        @PrimaryKey @NonNull val id: String = UUID.randomUUID().toString(),
        @ColumnInfo(name = "name_title") val nameTitle: String,
        @ColumnInfo(name = "name_first") val nameFirst: String,
        @ColumnInfo(name = "name_last") val nameLast: String,
        @ColumnInfo(name = "email") val email: String,
        @ColumnInfo(name = "picture_large") val pictureLarge: String,
        @ColumnInfo(name = "picture_medium") val pictureMedium: String,
        @ColumnInfo(name = "picture_thumbnail") val pictureThumbnail: String

)