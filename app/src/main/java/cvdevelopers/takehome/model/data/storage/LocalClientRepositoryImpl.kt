package cvdevelopers.takehome.model.data.storage

import cvdevelopers.takehome.entity.Client
import cvdevelopers.takehome.entity.Id
import cvdevelopers.takehome.entity.Name
import cvdevelopers.takehome.entity.Picture
import cvdevelopers.takehome.model.data.storage.room.ClientDao
import cvdevelopers.takehome.model.data.storage.room.DatabaseClient
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LocalClientRepositoryImpl(
        private val clientDao: ClientDao
): LocalClientRepository {

    override fun saveClientList(clientList: List<Client>): Completable {
        return Completable.fromAction {
            val databaseList = clientList.map { it.toDatabase() }
            clientDao.updateClients(databaseList)
        }
        .subscribeOn(Schedulers.io())
    }

    override fun getClientList(): Single<List<Client>> {
        return clientDao.getClientList()
                .subscribeOn(Schedulers.io())
                .map { items ->
                    items.map { it.toEntity() }
                }
    }

    private fun Client.toDatabase(): DatabaseClient {
        return DatabaseClient(
                nameTitle = name.title,
                nameFirst = name.first,
                nameLast = name.last,
                email = email,
                pictureLarge = picture.large,
                pictureMedium = picture.medium,
                pictureThumbnail = picture.thumbnail
        )
    }

    private fun DatabaseClient.toEntity(): Client {
        return Client(
                id = Id(
                        name = "",
                        value = id
                ),
                name = Name(
                        first = nameFirst,
                        last = nameLast,
                        title = nameTitle
                ),
                email = email,
                picture = Picture(
                        large = pictureLarge,
                        medium = pictureMedium,
                        thumbnail = pictureThumbnail
                )
        )
    }
}