package cvdevelopers.takehome.model.data.storage.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DatabaseClient::class],
        exportSchema = false,
        version = 1)
abstract class LocalDatabase : RoomDatabase() {

    abstract fun getClientDao(): ClientDao

}