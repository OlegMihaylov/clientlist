package cvdevelopers.takehome.model.data.server

import cvdevelopers.takehome.entity.Client
import io.reactivex.Single

class RemoteClientRepositoryImpl(
        private val api: RandomUserApiEndpoint
): RemoteClientRepository {

    override fun getClientList(page: Int, pageSize: Int): Single<List<Client>> {
        return api.getClient(page, pageSize)
                .map { it.results }
    }

}