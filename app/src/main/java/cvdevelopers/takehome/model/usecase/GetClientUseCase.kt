package cvdevelopers.takehome.model.usecase

import cvdevelopers.takehome.entity.Client
import cvdevelopers.takehome.model.data.server.RemoteClientRepository
import cvdevelopers.takehome.model.data.storage.LocalClientRepository
import io.reactivex.Single

class GetClientUseCase(
        private val localClientRepository: LocalClientRepository,
        private val remoteClientRepository: RemoteClientRepository
) {

    fun getClientList(): Single<List<Client>> {
        return localClientRepository.getClientList()
                .flatMap { databaseList ->
                    if(databaseList.isEmpty()) {
                        reloadClientListFromNetwork()
                    } else {
                        Single.fromCallable { databaseList }
                    }
                }
    }

    fun reloadClientListFromNetwork(): Single<List<Client>> {
        return remoteClientRepository.getClientList(page = PAGE_NUMBER, pageSize = PAGE_SIZE)
                .flatMapCompletable {
                    localClientRepository.saveClientList(it)
                }
                .andThen(localClientRepository.getClientList())
    }

    companion object {
        const val PAGE_NUMBER = 0
        const val PAGE_SIZE = 15
    }

}