package cvdevelopers.takehome.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cvdevelopers.takehome.ViewModelFactory
import cvdevelopers.takehome.ui.ClientListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ClientListViewModel::class)
    abstract fun bindClientListViewModule(clientListViewModel: ClientListViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}