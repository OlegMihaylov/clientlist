package cvdevelopers.takehome.dagger

import cvdevelopers.takehome.LuminaryTakeHomeApplication
import cvdevelopers.takehome.ui.ClientItemViewHolder
import cvdevelopers.takehome.ui.ClientListFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by CamiloVega on 10/7/17.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetworkClientModule::class, ViewModelModule::class))
interface ApplicationComponent {

    fun inject(app: LuminaryTakeHomeApplication)
    fun inject(clientListFragment: ClientListFragment)
    fun inject(clientItemViewHolder: ClientItemViewHolder)
}