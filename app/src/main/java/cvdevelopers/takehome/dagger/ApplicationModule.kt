package cvdevelopers.takehome.dagger

import android.app.Application
import androidx.room.Room
import cvdevelopers.takehome.LuminaryTakeHomeApplication
import cvdevelopers.takehome.model.data.server.RandomUserApiEndpoint
import cvdevelopers.takehome.model.data.server.RemoteClientRepository
import cvdevelopers.takehome.model.data.server.RemoteClientRepositoryImpl
import cvdevelopers.takehome.model.data.storage.LocalClientRepository
import cvdevelopers.takehome.model.data.storage.LocalClientRepositoryImpl
import cvdevelopers.takehome.model.data.storage.room.LocalDatabase
import cvdevelopers.takehome.model.usecase.GetClientUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule(private val app: LuminaryTakeHomeApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun localClientRepository(localDatabase: LocalDatabase): LocalClientRepository {
        return LocalClientRepositoryImpl(localDatabase.getClientDao())
    }

    @Provides
    @Singleton
    fun remoteClientRepository(apiEndpoint: RandomUserApiEndpoint): RemoteClientRepository {
        return RemoteClientRepositoryImpl(apiEndpoint)
    }

    @Provides
    @Singleton
    fun getClientUseCase(localClientRepository: LocalClientRepository, remoteClientRepository: RemoteClientRepository): GetClientUseCase {
        return GetClientUseCase(localClientRepository, remoteClientRepository)
    }

    @Provides
    @Singleton
    fun localDatabase(): LocalDatabase {
        return Room.databaseBuilder(app,
                LocalDatabase::class.java, "localdatabase")
                .build()
    }

}