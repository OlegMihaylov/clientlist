package cvdevelopers.takehome.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import cvdevelopers.takehome.entity.Client

class ClientsAdapter : ListAdapter<Client, ClientItemViewHolder>(ClientDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientItemViewHolder {
        return ClientItemViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: ClientItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }
}

class ClientDiffCallback : DiffUtil.ItemCallback<Client>() {
    override fun areItemsTheSame(oldItem: Client, newItem: Client): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Client, newItem: Client): Boolean {
        return oldItem == newItem
    }

}