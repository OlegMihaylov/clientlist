package cvdevelopers.takehome.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cvdevelopers.githubstalker.R
import cvdevelopers.takehome.LuminaryTakeHomeApplication
import cvdevelopers.takehome.entity.Client
import cvdevelopers.takehome.entity.Name
import cvdevelopers.takehome.utils.image.ImageLoader
import javax.inject.Inject

class ClientItemViewHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {

    @Inject
    lateinit var imageLoader: ImageLoader

    init {
        LuminaryTakeHomeApplication.appComponent.inject(this)
    }

    private val imageView = view.findViewById<ImageView>(R.id.client_image)
    private val nameTextView = view.findViewById<TextView>(R.id.client_name)


    fun bind(client: Client) {
        imageLoader.loadCircularImage(client.picture.thumbnail, imageView)
        nameTextView.text = client.name.format()
    }

    private fun Name.format(): String = "$title $last $first"

    companion object {

        fun getInstance(parent: ViewGroup): ClientItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.view_client_item, parent, false)
            return ClientItemViewHolder(view)
        }

    }

}