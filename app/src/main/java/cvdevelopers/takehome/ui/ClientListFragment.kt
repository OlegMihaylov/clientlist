package cvdevelopers.takehome.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import cvdevelopers.githubstalker.R
import cvdevelopers.takehome.LuminaryTakeHomeApplication
import kotlinx.android.synthetic.main.client_list_fragment.*
import javax.inject.Inject

class ClientListFragment : Fragment(R.layout.client_list_fragment) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ClientListViewModel> { viewModelFactory }
    private val clientsAdapter = ClientsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LuminaryTakeHomeApplication.appComponent.inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeViews()
        initializeDataObservers()
    }

    private fun initializeViews() {
        client_list.adapter = clientsAdapter
        refresh_layout.setOnRefreshListener { viewModel.onRefresh() }
    }

    private fun initializeDataObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {isLoading ->
            loader_block.visibility = if(isLoading) View.VISIBLE else View.GONE
        })
        viewModel.isError.observe(viewLifecycleOwner, Observer {isError ->
            load_error_block.visibility = if(isError) View.VISIBLE else View.GONE
        })
        viewModel.isRefreshError.observe(viewLifecycleOwner, Observer {isRefreshError ->
            if (isRefreshError) {
                Toast.makeText(requireContext(), R.string.update_error, Toast.LENGTH_SHORT).show()
            }
        })
        viewModel.clientList.observe(viewLifecycleOwner, Observer {
            load_error_block.visibility = View.GONE
            clientsAdapter.submitList(it)
        })
        viewModel.isRefresh.observe(viewLifecycleOwner, Observer {
            refresh_layout.isRefreshing = it
        })
    }


    companion object {
        fun newInstance() = ClientListFragment()
    }

}