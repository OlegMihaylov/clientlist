package cvdevelopers.takehome.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cvdevelopers.takehome.entity.Client
import cvdevelopers.takehome.model.usecase.GetClientUseCase
import cvdevelopers.takehome.utils.LiveEvent
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ClientListViewModel @Inject constructor(
    private val getClientUseCase: GetClientUseCase
) : ViewModel() {

    val clientList = MutableLiveData<List<Client>>()
    val isLoading = MutableLiveData(false)
    val isRefresh = MutableLiveData(false)
    val isError = MutableLiveData(false)
    val isRefreshError = LiveEvent<Boolean>()
    var loadDisposable: Disposable? = null

    init {
        loadUserList()
    }

    private fun loadUserList() {
        isLoading.postValue(true)
        loadDisposable = getClientUseCase.getClientList().subscribe({
            isLoading.postValue(false)
            clientList.postValue(it)
        }, {
            isError.postValue(true)
            isLoading.postValue(false)
        })
    }

    fun onRefresh() {
        isRefresh.postValue(true)
        loadDisposable = getClientUseCase.reloadClientListFromNetwork().subscribe({
            isRefresh.postValue(false)
            clientList.postValue(it)
        }, {
            isRefresh.postValue(false)
            isRefreshError.postValue(true)
        })
    }

    override fun onCleared() {
        loadDisposable?.dispose()
    }

}