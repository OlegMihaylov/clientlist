package cvdevelopers.takehome

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cvdevelopers.githubstalker.R
import cvdevelopers.takehome.ui.ClientListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.activity_fragment_container, ClientListFragment.newInstance())
                    .commitNow()
        }
    }


}
